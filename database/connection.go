package database

import (
	"github.com/labstack/gommon/log"
	"gopkg.in/mgo.v2"
)

const (
	DB_CONNECTION = "mongodb://localhost"
	DB_NAME       = "cloner-test"
)

type mongoSession struct {
	session *mgo.Session
}

var session *mongoSession

func init() {
	getInstance()
}

func getInstance() *mongoSession {
	if session == nil {
		session = &mongoSession{createMongoSesion()}
	}
	return session
}

func createMongoSesion() *mgo.Session {

	session, err := mgo.Dial(DB_CONNECTION)
	if err != nil {
		log.Fatal("Error al conectar con mongo DB")
	}
	return session
}

func CloseSession() {
	getInstance().session.Close()
}

func GetCollection(collectionName string) *mgo.Collection {
	return getInstance().session.DB(DB_NAME).C(collectionName)
}
