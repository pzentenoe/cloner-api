package database

import (
	"errors"
	"gitlab.com/pzentenoe/cloner-api/models"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const VERSION_COLLECTION = "versiones"

func CreateVersion(fileName string) (models.Version, error) {
	version := models.Version{}
	version.ID = bson.NewObjectId()
	version.NombreArchivo = fileName
	version.CreatedAt = time.Now()
	version.FormatearFechaCreacion()
	version.Estado = true

	if err := GetCollection(VERSION_COLLECTION).Insert(version); err != nil {
		return models.Version{}, errors.New("Error al crear version")
	}
	return version, nil

}

func FindVersionByFilenameAndDate(fileName, when string) (models.Version, error) {
	var version models.Version
	query := bson.M{"nombre": fileName, "fecha_creacion": bson.M{"$eq": when},}
	err := GetCollection(VERSION_COLLECTION).Find(query).One(&version)
	if err != nil {
		return version, errors.New("Error al obtener version")
	} else {
		return version, nil
	}
}

func DeleteVersion(fileName string) error {
	query := bson.M{"nombre_archivo": fileName}
	err := GetCollection(DIRECTORIO_COLLECTION).Remove(query)
	return err
}
