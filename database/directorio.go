package database

import (
	"errors"
	"gitlab.com/pzentenoe/cloner-api/models"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const DIRECTORIO_COLLECTION = "directorios"

func CreateDirectorio(path string) (models.Directorio, error) {

	//Validar si existe un directorio creado en la BD
	directorio, _ := FindDirectorioByPath(path)
	if directorio.Nombre != "" {
		return directorio, nil
	}
	directorio.CreatedAt = time.Now()
	directorio.FechaActualizacion = directorio.CreatedAt
	directorio.FormatearFechaCreacion()
	directorio.Nombre = path
	directorio.Versiones = models.Versiones{}
	directorio.ID = bson.NewObjectId()
	if err := GetCollection(DIRECTORIO_COLLECTION).Insert(directorio); err != nil {
		return models.Directorio{}, errors.New("No se pudo crear directorio")
	}
	return directorio, nil
}

func DeleteDirectorio(path string) error {
	query := bson.M{"nombre": path,}
	if err := GetCollection(DIRECTORIO_COLLECTION).Remove(query); err != nil {
		return errors.New("No se pudo eliminar directorio")
	}
	return nil
}

func FindContentDirectorio(path, when string) (models.Directorio, error) {
	directorio := models.Directorio{}
	query := bson.M{"nombre": path, "versiones": bson.M{"$elemMatch": bson.M{"fecha_creacion": bson.M{"$eq":when}}, "$sort": bson.M{"nombre_archivo": "-1"}}}
	if err := GetCollection(DIRECTORIO_COLLECTION).Find(query).One(&directorio); err != nil {
		return models.Directorio{}, errors.New("No se pudo obtener el directorio con sus archivos")
	}
	return directorio, nil
}

func FindDirectorioByPath(path string) (models.Directorio, error) {
	var directorio models.Directorio
	query := bson.M{"nombre": path,}
	err := GetCollection(DIRECTORIO_COLLECTION).Find(query).One(&directorio)
	if err != nil {
		return directorio, errors.New("Error al obtener directorio")
	} else {
		return directorio, nil
	}
}

func UpdateDirectorio(directorio models.Directorio) (models.Directorio, error) {
	directorio.FechaActualizacion = time.Now()
	document := bson.M{"_id": directorio.ID,}
	change := bson.M{"$set": directorio}
	error := GetCollection(DIRECTORIO_COLLECTION).Update(document, change)

	if error != nil {
		return directorio, error
	} else {
		return directorio, nil
	}
}
