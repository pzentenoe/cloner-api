package routes

import (
	"github.com/labstack/echo"
	"gitlab.com/pzentenoe/cloner-api/constants"
	"gitlab.com/pzentenoe/cloner-api/handlers"
)

func addVersionRoutes(e *echo.Echo) {
	e.GET(constants.URL_BASE+"/versiones", handlers.VersionList)
	e.POST(constants.URL_BASE+"/versiones", handlers.AddFile)
	e.DELETE(constants.URL_BASE+"/versiones", handlers.DeleteFile)
	e.GET(constants.URL_BASE+"/versiones/exist", handlers.Exists)
}
