package routes

import (
	"github.com/labstack/echo"
	"gitlab.com/pzentenoe/cloner-api/constants"
	"gitlab.com/pzentenoe/cloner-api/handlers"
)

func addDirectorioRoutes(e *echo.Echo)  {
	e.GET(constants.URL_BASE+"/directorios", handlers.ListDir)
	e.POST(constants.URL_BASE+"/directorios", handlers.AddFolder)
	e.DELETE(constants.URL_BASE+"/directorios", handlers.DeleteFolder)
}
