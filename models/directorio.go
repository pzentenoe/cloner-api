package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type (
	Directorio struct {
		ID                 bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
		Nombre             string        `bson:"nombre" json:"nombre"`
		Versiones          []Version     `bson:"versiones" json:"versiones"`
		CreatedAt          time.Time     `bson:"created_at" json:"created_at"`
		FechaCreacion      string        `bson:"fecha_creacion" json:"fecha_creacion"`
		FechaActualizacion time.Time     `bson:"fecha_actualizacion" json:"fecha_actualizacion, omitempty"`
	}
	Directorios []Directorio
)

func (this *Directorio) FormatearFechaCreacion() {
	this.FechaCreacion = this.CreatedAt.Format("02-01-2006 150405")
}
