package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type (
	Version struct {
		ID                bson.ObjectId `bson:"_id,omitempty" json:"id,omitempty"`
		NombreArchivo     string        `bson:"nombre_archivo" json:"nombre_archivo"`
		CreatedAt         time.Time     `bson:"created_at" json:"created_at"`
		FechaCreacion     string        `bson:"fecha_creacion" json:"fecha_creacion"`
		Estado            bool          `bson:"estado" json:"estado"`
	}
	Versiones []Version
)

func (this *Version) FormatearFechaCreacion() {
	this.FechaCreacion = this.CreatedAt.Format("02-01-2006 150405")
}
