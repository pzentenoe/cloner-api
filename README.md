# Proyecto cloner 

## Intalaciones necesarias para correr proyecto

- Tener instalado [docker](https://www.docker.com/get-started)
- Tener instalador [go](https://golang.org/doc/install)

## Pasos

- Instalar dependencias de golang
> Copiar el proyecto en el GOPATH (mirar documentacion [GOPATH](https://github.com/golang/go/wiki/SettingGOPATH))

```sh
$ go get -u github.com/labstack/echo/...
$ go get -u gopkg.in/mgo.v2
```
    
- Levantar base de datos mongodb con docker

    >docker run -d  -p 27017:27017 -p 28017:28017 --name mongodb mongo:latest mongod  
    
- Correr aplicacion 
```sh
$ go run main.go
```

- Otra forma es contruir aplicacion
```sh
$ go build -o cloner-api
```

- Correr ejecutable en linux o mac
>Abrir un terminal en el directorio del proyecto y ejecutar ./cloner-api

- Correr ejecutable en windows
>doble click al cloner-api.exe

	Titulo:	cloner-api  
	Autor: Pablo Zenteno 
	Fecha:	27-10-2018 
