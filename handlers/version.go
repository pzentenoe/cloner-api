package handlers

import (
	"errors"
	"github.com/labstack/echo"
	"gitlab.com/pzentenoe/cloner-api/database"
	"gitlab.com/pzentenoe/cloner-api/models"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"time"
)

func AddFile(c echo.Context) error {
	file, err := c.FormFile("file")
	if existError(err) {
		return c.JSON(http.StatusBadRequest, echo.Map{"message": "Erro al obtener archivo",})
	}
	path := c.QueryParam("path")

	fileName, err := createFile(file, path)
	if existError(err) {
		return c.JSON(http.StatusBadRequest, echo.Map{"message": "Erro al crear archivo",})
	}

	version, err := database.CreateVersion(fileName)
	if existError(err) {
		//Eliminar archivo creado si fallo el guardado en BD
		deleteFile(path)
	}
	//Buscar directorio en BD y agregar una version
	directorio, err := database.FindDirectorioByPath(path)
	directorio.Versiones = append(directorio.Versiones, version)
	database.UpdateDirectorio(directorio)

	return c.JSON(http.StatusCreated, version)

}

func createFile(file *multipart.FileHeader, path string) (string, error) {

	var extension = filepath.Ext(file.Filename)
	var fileName = file.Filename[0 : len(file.Filename)-len(extension)]

	tiempo := strconv.Itoa(int(getMiliSecondsNow()))
	fileName = fileName + tiempo + extension

	path = validateFinalPath(path)
	pathArchivo := path + fileName
	src, err := file.Open()
	if existError(err) {
		return fileName, errors.New("Error al abrir archivo " + file.Filename)
	}
	defer src.Close()

	dst, err := os.Create(pathArchivo)
	if err != nil {
		return fileName, errors.New("Error al crear archivo")
	}
	defer dst.Close()

	if _, err = io.Copy(dst, src); err != nil {
		return fileName, errors.New("Error al guardar archivo")
	}
	return fileName, nil
}

func getMiliSecondsNow() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func validateFinalPath(path string) string {
	var ultimoCaracter = path[len(path)-1:]
	if ultimoCaracter != "/" {
		path += "/"
	}
	return path
}

func DeleteFile(c echo.Context) error {
	pathFile := c.QueryParam("path")
	fileName := path.Base(pathFile)
	folderName := path.Dir(pathFile)
	if err := deleteFile(pathFile); existError(err) {
		return c.JSON(http.StatusBadRequest, echo.Map{"message": "Error al eliminar archivo",})
	}
	directorio, _ := database.FindDirectorioByPath(folderName)

	database.DeleteVersion(fileName)
	removeVersionFromDirectorio(&directorio, fileName)
	database.UpdateDirectorio(directorio)
	return c.JSON(http.StatusAccepted, true)
}

func Exists(c echo.Context) error {
	pathFile := c.QueryParam("path")
	when := c.QueryParam("when")
	fileName := path.Base(pathFile)
	version, err := database.FindVersionByFilenameAndDate(fileName, when)
	if existError(err) || version.NombreArchivo == "" {
		return c.JSON(http.StatusNotFound, false)

	}
	return c.JSON(http.StatusOK, true)

}

func VersionList(c echo.Context) error {
	path :=  c.QueryParam("path")
	start := c.QueryParam("start")
	end := c.QueryParam("end")

	startTime := parseStringToTime(start)
	EndTime := parseStringToTime(end)

	directorio, _ := database.FindDirectorioByPath(path)

	var versiones models.Versiones = []models.Version{}
	for _, version := range directorio.Versiones {
		if version.CreatedAt.After(startTime) && version.CreatedAt.Before(EndTime) {
			versiones = append(versiones, version)
		}
	}
	return c.JSON(http.StatusOK, versiones)
}

func parseStringToTime(date string) time.Time {
	when_time, _ := time.Parse("02-01-2006 15:04:05", date)
	return when_time
}

func removeVersionFromDirectorio(directorio *models.Directorio, fileName string) {
	for i, version := range directorio.Versiones {
		if version.NombreArchivo == fileName {
			directorio.Versiones = append(directorio.Versiones[:i], directorio.Versiones[i+1:]...)
			break
		}
	}
}

func deleteFile(path string) error {
	var err = os.Remove(path)
	if existError(err) {
		return err
	}
	return nil
}

func existError(err error) bool {
	return err != nil
}
