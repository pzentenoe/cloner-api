package handlers

import (
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	"gitlab.com/pzentenoe/cloner-api/database"
	"net/http"
	"os"
)

func AddFolder(c echo.Context) error {

	path := c.QueryParam("path")
	if err := createFolder(path); err != nil {
		return c.JSON(http.StatusBadGateway, echo.Map{"message": "Error al crear directorio"})
	}
	directorio_created, err := database.CreateDirectorio(path)
	if err != nil {
		//Borrar carpeta creada en caso de que fallo la creacion del directorio en la base de datos
		deleteFolder(path)
		c.JSON(http.StatusBadGateway, echo.Map{"message": err.Error()})
	}
	return c.JSON(http.StatusCreated, directorio_created)
}

func createFolder(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		erro := os.MkdirAll(path, os.ModePerm)
		if erro != nil {
			log.Error("No se pudo crear directorio")
		}
		log.Info("Directorio creado")
		return nil
	} else {
		return err
	}
}

func DeleteFolder(c echo.Context) error {
	path := c.QueryParam("path")
	if err := deleteFolder(path); err != nil {
		return c.JSON(http.StatusBadGateway, echo.Map{"message": "Error al eliminar directorio"})
	}
	if err := database.DeleteDirectorio(path); err != nil {
		//Restaurar carpeta eliminada en caso de fallar la eliminacion en la BD
		createFolder(path)

		return c.JSON(http.StatusBadGateway, echo.Map{"message": "Error al eliminar directorio"})
	}
	return c.JSON(http.StatusNoContent, true)
}

func deleteFolder(path string) (error) {
	err := os.RemoveAll(path)
	if err != nil {
		log.Error("No se pudo eliminar directorio")
		return err
	}
	return nil
}

func ListDir(c echo.Context) error {

	path := c.QueryParam("path")
	when := c.QueryParam("when")

	directorio, err := database.FindContentDirectorio(path, when)
	if err != nil {
		return c.JSON(http.StatusBadGateway, echo.Map{"message": err.Error()})
	}
	lista_archivos := make([]string, 1)
	for _, version := range directorio.Versiones {
		lista_archivos = append(lista_archivos, version.NombreArchivo)
	}
	return c.JSON(http.StatusOK, lista_archivos)

}
